Rails.application.routes.draw do
  resources :arrivals
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  
  resources :properties
  resources :bookings
  resources :guests
  resources :cities
  resources :units
  resources :unit_types
  #get 'admin', to: 'admins#index'
  #get 'admin/:id', to: 'admins#show'
  #post 'admin/create', to: 'admins#create'
  #put 'admin/edit/:id', to: 'admins#update'
  #delete 'admin/:id', to: 'admins#delete'

  #get 'booking', to: 'bookings#index'
  #get 'booking/:id', to: 'bookings#show'
  #post 'booking/create', to: 'bookings#create'
  #put 'booking/edit/:id', to: 'bookings#update'
  #delete 'booking/:id', to: 'bookings#delete'

  #get 'guest', to: 'guests#index'
  #get 'guest/:id', to: 'guests#show'
  #post 'guest/create', to: 'guests#create'
  #put 'guest/edit/:id', to: 'guests#update'
  #delete 'guest/:id', to: 'guests#delete'

  #get 'property', to: 'properties#index'
  #get 'property/:id', to: 'properties#show'
  #post 'property/create', to: 'properties#create'
  #put 'property/edit/:id', to: 'properties#update'
  #delete 'property/:id', to: 'properties#delete'

  #get 'city', to: 'cities#index'
  #get 'city/:id', to: 'cities#show'
  #post 'cityt/create', to: 'cities#create'
  #put 'city/edit/:id', to: 'cities#update'
  #delete 'city/:id', to: 'cities#delete'

  #get 'unit', to: 'units#index'
  #get 'unit/:id', to: 'units#show'
  #post 'unit/create', to: 'units#create'
  #put 'unit/edit/:id', to: 'units#update'
  #delete 'unit/:id', to: 'units#delete'

  #get 'type', to: 'units#index'
  #get 'unit/:id', to: 'units#show'
  #post 'unit/create', to: 'units#create'
  #put 'unit/edit/:id', to: 'units#update'
  #delete 'unit/:id', to: 'units#delete'
end
