class CreateProperties < ActiveRecord::Migration[5.2]
  def change
    create_table :properties do |t|
      t.string :name
      t.text :address
      t.references :city, foreign_key: true

      t.timestamps
    end
  end
end
