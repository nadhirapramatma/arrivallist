class CreateUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :units do |t|
      t.string :name
      t.text :description
      t.references :property, foreign_key: true
      t.references :unit_type, foreign_key: true

      t.timestamps
    end
  end
end
