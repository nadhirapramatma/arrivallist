class CreateArrivals < ActiveRecord::Migration[5.2]
  def change
    create_table :arrivals do |t|
      t.references :guest, foreign_key: true
      t.references :unit, foreign_key: true
      t.date :checkin
      t.date :checkout
      t.references :status, foreign_key: true

      t.timestamps
    end
  end
end
