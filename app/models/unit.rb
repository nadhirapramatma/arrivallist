class Unit < ApplicationRecord
  belongs_to :property
  belongs_to :unit_type
end
