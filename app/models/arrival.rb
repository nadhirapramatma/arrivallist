class Arrival < ApplicationRecord
  belongs_to :guest
  belongs_to :unit
  belongs_to :status
end
