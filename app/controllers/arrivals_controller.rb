class ArrivalsController < InheritedResources::Base

  private

    def arrival_params
      params.require(:arrival).permit(:guest_id, :unit_id, :checkin, :checkout, :status_id)
    end
end

