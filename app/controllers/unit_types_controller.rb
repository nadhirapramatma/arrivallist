class UnitTypesController < ApplicationController
    before_action :set_unit_type, only: [ :show, :update, :delete ]

    def index
      @unit_types = UnitType.all
      render json: { result: true, unit_types: unit_types }, status: :ok
    end
      
    def show
      @unit_type = UnitType.find_by(id: params[:id])
	    render json: unit_type.nil? ? { data: "type not found"} : unit_type, status: :ok
    end
      
    def create
      unit_type = unit_type.create(unit_type_params)
      if unit_type.save
        render json: { result: true, unit_type: unit_type }, status: :created
      else
        render json: { result: false, unit_type: unit_type.errors }, status: :unprocessable_entity
      end
    end
      
    def update
      if @unit_type.update(unit_type_params)
        render json: { result: true, msg: "Update Success" }
      else  
        render json: { result: false, msg: "Update Failed" }
      end
    end
      
    def delete
      if @unit_type.destroy
        render json: { result: true, msg: "Delete Success" }
      else
        render json: { result: false, msg: "Delete Failed" }
      end
    end
      
    private

 
    def unit_type_params
      params.permit(:unit_type).require(:name)
    end
      
    def set_unit_type
      @unit_type = UnitType.find(params[:id])
    end
end
