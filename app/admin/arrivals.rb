ActiveAdmin.register Arrival do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
    permit_params :guest_id, :unit_id, :checkin, :checkout, :status_id
end
