json.extract! arrival, :id, :guest_id, :unit_id, :checkin, :checkout, :status_id, :created_at, :updated_at
json.url arrival_url(arrival, format: :json)
