# Arrival List

ERD can be accessed using this link: https://drive.google.com/file/d/1NNsSplPWwcrdH0zm0A2nJdwR_e_zyyTo/view?usp=sharing
* Ruby version: ruby 2.5.1
* Rails version: Rails 5.2.1
* Database: Postgresql
* Additional gemfile: Activeadmin 1.3.1
* Admin Authentification: Devise
